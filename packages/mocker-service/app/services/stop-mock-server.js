const mockserver = require('mockserver-node');

const stopMockServer = async ({port = 9999}) => {
    await mockserver.stop_mockserver({
        serverPort: port
    });
};

module.exports = stopMockServer;
