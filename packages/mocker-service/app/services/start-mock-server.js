const mockserver = require('mockserver-node');

const startMockServer = async ({port = 9999}) => {
    await mockserver.start_mockserver({
        serverPort: port,
        trace: true
    });
};

module.exports = startMockServer;
