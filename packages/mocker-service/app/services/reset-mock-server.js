const mockServerClient = require('mockserver-client').mockServerClient;
const ErrorUtils = require('../utils/on-error');

const resetMockServer = ({host = 'localhost', port = 9999, contextPath = ''}) => {
    mockServerClient(host, port, contextPath)
        .reset()
        .then(() => {
                console.log('Reset all mock expectations');
            }, error => {
                ErrorUtils.log({error: error, errLabel: 'Following error occurred while resetting the expectations'});
            }
        );
};

module.exports = resetMockServer;
