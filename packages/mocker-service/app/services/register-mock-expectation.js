const mockServerClient = require('mockserver-client').mockServerClient;
const configs = require('../configs/configs');
const ErrorUtils = require('../utils/on-error');
const buildMockRequest = require('../utils/mock-request-builder');
const buildMockResponse = require('../utils/mock-response-builder');

const host = configs.mockServerHost;
const port = configs.mockServerPort;

module.exports = async (expectationConfigs) => {
    const mockReqConfigs = buildMockRequest(expectationConfigs);
    const mockResConfigs = await buildMockResponse(expectationConfigs);

    try {
        await mockServerClient(host, port).clear(mockReqConfigs);

        await mockServerClient(host, port)
            .mockAnyResponse({
                "httpRequest": mockReqConfigs,
                "httpResponse": mockResConfigs
            });

        console.log(`Successfully registered expectation for ${expectationConfigs.name} with URI :: ${expectationConfigs.path}`);
    } catch (error) {
        ErrorUtils.log({
            error: error,
            errLabel: `Failed to register expectation for ${expectationConfigs.name} with URI :: ${expectationConfigs.path}`
        });
    }
};
