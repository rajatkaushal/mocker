const mockServerClient = require('mockserver-client').mockServerClient;
const configs = require('../configs/configs');
const ErrorUtils = require('../utils/on-error');
const buildMockRequest = require('../utils/mock-request-builder');

const host = configs.mockServerHost;
const port = configs.mockServerPort;

module.exports = async (expectationConfigs) => {
    const mockReqConfigs = buildMockRequest(expectationConfigs);

    try {
        await mockServerClient(host, port).clear(mockReqConfigs);

        console.log(`Successfully cleared expectation for ${expectationConfigs.name} with URI :: ${expectationConfigs.path}`);
    } catch (error) {
        ErrorUtils.log({
            error: error,
            errLabel: `Failed to clear expectation for ${expectationConfigs.name} with URI :: ${expectationConfigs.path}`
        });
    }
};
