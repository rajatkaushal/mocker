const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DelaySchema = new Schema({
    timeUnit: { type: String, enum: ['SECONDS', 'MINUTES'], default: 'SECONDS' },
    value: { type: Number, default: 0 }
});

const schema = new Schema({
    id: Schema.Types.ObjectId,
    type: { type: String, enum: [null, '', 'json', 'xml'], default: 'json' },
    isList: { type: Boolean, default: false },
    isRandom: { type: Boolean, default: false },
    randomCount: Number,
    statusCode: { type: Number, default: 200 },
    reasonPhrase: String,
    delay: DelaySchema,
    headers: Object,
    xmlRootName: String,
    isHeadlessXML: { type: Boolean, default: true },
    body: { type: Object, required: true }
});


module.exports = mongoose.model('responseConfigs', schema);
