const mongoose = require('mongoose');
const RequestConfigsModel = require('./request-configs');
const ResponseConfigsModel = require('./response-configs');
const Schema = mongoose.Schema;


const schema = new Schema({
    id: Schema.Types.ObjectId,
    name: { type: String, minlength: 5, maxlength: 25, trim: true, unique: true, required: true },
    path: { type: String, minlength: 5, trim: true, required: true },
    method: { type: String, enum: ['GET', 'PUT', 'PATCH', 'POST', 'DELETE', 'P.*{2,3}', 'P.*{2,4}'], default: 'GET', required: true },
    description: { type: String, maxlength: 250, trim: true },
    requestConfigs: { type: mongoose.Schema.Types.ObjectId, ref: 'requestConfigs' },
    responseConfigs: { type: mongoose.Schema.Types.ObjectId, ref: 'responseConfigs', required: true }
});


schema.statics.create = async (reqData) => {
    if (reqData.requestConfigs) {
        reqData.requestConfigs = (await new RequestConfigsModel(reqData.requestConfigs).save())._id;
    }
    reqData.responseConfigs = (await new ResponseConfigsModel(reqData.responseConfigs).save())._id;

    return await new ExpectationsModel(reqData).save();
};

schema.statics.update = async (id, reqData) => {
    const oldSavedExpectation = await ExpectationsModel.findById(id);
    reqData._id = id;

    if (oldSavedExpectation.requestConfigs && oldSavedExpectation.requestConfigs._id) {
        reqData.requestConfigs._id = oldSavedExpectation.requestConfigs._id;
        await RequestConfigsModel.findByIdAndUpdate(oldSavedExpectation.requestConfigs._id, reqData.requestConfigs);
    } else {
        reqData.requestConfigs = (await new RequestConfigsModel(reqData.requestConfigs).save())._id;
    }

    if (oldSavedExpectation.responseConfigs && oldSavedExpectation.responseConfigs._id) {
        reqData.responseConfigs._id = oldSavedExpectation.responseConfigs._id;
        await ResponseConfigsModel.findByIdAndUpdate(oldSavedExpectation.responseConfigs._id, reqData.responseConfigs);
    } else {
        reqData.responseConfigs = (await new ResponseConfigsModel(reqData.responseConfigs).save())._id;
    }

    return await ExpectationsModel.findByIdAndUpdate(id, reqData);
};

schema.statics.delete = async (id) => {
    const expectation = await ExpectationsModel.findById(id);

    if (expectation.requestConfigs && expectation.requestConfigs._id) {
        await RequestConfigsModel.findByIdAndDelete(expectation.requestConfigs._id);
    }
    await ResponseConfigsModel.findByIdAndDelete(expectation.responseConfigs._id);

    return await ExpectationsModel.findByIdAndDelete(id);
};


const ExpectationsModel = mongoose.model('expectations', schema);
module.exports = ExpectationsModel;
