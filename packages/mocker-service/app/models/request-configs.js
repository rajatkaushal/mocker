const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    id: Schema.Types.ObjectId,
    queryParams: Object,
    headers: Object,
    cookies: Object,
    body: Object,
    type: { type: String, enum: [null, '', 'json', 'xml'], default: 'json' },
    matchType: { type: String, enum: [null, '', 'STRICT', 'ONLY_MATCHING_FIELDS'] }
});


module.exports = mongoose.model('requestConfigs', schema);
