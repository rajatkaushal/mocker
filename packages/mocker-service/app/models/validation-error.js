module.exports = class ValidationError {
    constructor(message, code = 'BAD_REQUEST', statusCode = 400) {
        this.status = statusCode;
        this.message = message;
        this.code = code;
    }
};
