const express = require('express');
const expectationsStore = require('../store/expectations');
const clearMockExpectation = require('../services/clear-mock-expectation');
const registerMockExpectation = require('../services/register-mock-expectation');
const router = express.Router();


router.get('/register/:expectationId', async (req, res) => {
    const expectationId = req.path.expectationId || req.params.expectationId;
    try {
        const expectation = await expectationsStore.getExpectationById(expectationId);
        await registerMockExpectation(expectation);
        res.send('Expectation Registered');
    } catch (err) {
        res.status(err.status).json(err);
    }
});

router.get('/reset/:expectationId', async (req, res) => {
    const expectationId = req.path.expectationId || req.params.expectationId;
    try {
        const expectation = await expectationsStore.getExpectationById(expectationId);
        await registerMockExpectation(expectation);
        res.send('Expectation Reset');
    } catch (err) {
        res.status(err.status).json(err);
    }
});

router.get('/remove/:expectationId', async (req, res) => {
    const expectationId = req.path.expectationId || req.params.expectationId;
    try {
        const expectation = await expectationsStore.getExpectationById(expectationId);
        await clearMockExpectation(expectation);
        res.send('Expectation Cleared');
    } catch (err) {
        res.status(err.status).json(err);
    }
});


module.exports = router;
