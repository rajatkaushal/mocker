const express = require('express');
const verifyRequest = require('../utils/verify-request');
const expectationsStore = require('../store/expectations');
const clearMockExpectation = require('../services/clear-mock-expectation');
const registerMockExpectation = require('../services/register-mock-expectation');
const router = express.Router();


router.post('/', async (req, res) => {
    try {
        verifyRequest(req.body);

        if (req.query.registerMock) {
            await registerMockExpectation(req.body);
        }

        res.send(await expectationsStore.createExpectation(req));
    } catch (err) {
        res.status(err.status).json(err);
    }
});

router.get('/', async (req, res) => {
    try {
        res.send(await expectationsStore.getExpectations(req));
    } catch (err) {
        res.status(err.status).json(err);
    }
});

router.get('/:id', async (req, res) => {
    const id = req.path.id || req.params.id;
    try {
        res.send(await expectationsStore.getExpectationById(id));
    } catch (err) {
        res.status(err.status).json(err);
    }
});

router.put('/:id', async (req, res) => {
    try {
        verifyRequest(req.body);

        if (req.query.registerMock) {
            await registerMockExpectation(req.body);
        }

        res.send(await expectationsStore.updateExpectationById(req));
    } catch (err) {
        res.status(err.status).json(err);
    }
});

router.delete('/:id', async (req, res) => {
    const id = req.path.id || req.params.id;
    try {
        const expectation = await expectationsStore.getExpectationById(id);
        await clearMockExpectation(expectation);
        res.send(await expectationsStore.deleteExpectationById(id));
    } catch (err) {
        res.status(err.status).json(err);
    }
});


module.exports = router;
