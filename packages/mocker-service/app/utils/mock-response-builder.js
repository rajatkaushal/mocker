const xml2js = require('xml2js');
const shuffle = require('./shuffle');
const MockHelper = require('./mock-helper');
const xmlParser = new xml2js.Parser();

module.exports = async (expectationConfigs) => {
    const resConfigs = expectationConfigs.responseConfigs;
    const httpResponse = {
        "statusCode": resConfigs.statusCode || 200,
        "headers": {
            "Content-Type": [ MockHelper.getMimeType(resConfigs.type) ],
            ...MockHelper.getConfigMatchers(resConfigs.headers)
        }
    };

    if (resConfigs.reasonPhrase) {
        httpResponse.reasonPhrase = resConfigs.reasonPhrase;
    }

    if (resConfigs.delay && resConfigs.delay.value > 0) {
        httpResponse.delay = {
            "timeUnit": resConfigs.delay.timeUnit,
            "value": resConfigs.delay.value
        };
    }

    switch (resConfigs.type) {
        case 'json': {
            let bodyData = typeof resConfigs.body === 'string' ? JSON.parse(resConfigs.body) : resConfigs.body;
            if (resConfigs.isList && bodyData instanceof Array && bodyData.length > 0) {
                if (resConfigs.isRandom) {
                    shuffle(bodyData);
                }
                bodyData = bodyData.slice(0, (resConfigs.randomCount || bodyData.length));
            }
            httpResponse.body = JSON.stringify(bodyData);
            break;
        }
        case 'xml': {
            let xmlBuilderConfigs = {headless: resConfigs.isHeadlessXML};
            if (resConfigs.xmlRootName) {
                xmlBuilderConfigs.rootName = resConfigs.xmlRootName;
            }
            const xmlBuilder = new xml2js.Builder(xmlBuilderConfigs);

            let bodyData = await xmlParser.parseStringPromise(resConfigs.body);
            if (resConfigs.isList && bodyData instanceof Array && bodyData.length > 0) {
                if (resConfigs.isRandom) {
                    shuffle(bodyData);
                }
                bodyData = bodyData.slice(0, (resConfigs.randomCount || bodyData.length));
            }
            httpResponse.body = `${xmlBuilder.buildObject(bodyData)}`;
            break;
        }
        default:
            httpResponse.body = `${resConfigs.body}`;
            break;
    }

    return httpResponse;
};
