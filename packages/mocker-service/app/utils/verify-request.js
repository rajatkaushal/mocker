const ValidationError = require('../models/validation-error');

module.exports = (requestData) => {
    if (requestData) {
        if (!requestData.name)
            throw new ValidationError('Missing: Request Name');
        if (!requestData.path)
            throw new ValidationError('Missing: Request Path');
        if (!requestData.method)
            throw new ValidationError('Missing: Request Method');
        if (!requestData.responseConfigs)
            throw new ValidationError('Missing: Response Specifications');
        else
            if (!requestData.responseConfigs.body)
                throw new ValidationError('Missing: Response Body');
    } else
        throw new ValidationError('Missing: Whole Specification');
};
