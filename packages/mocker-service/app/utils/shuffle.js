module.exports = (dataArray) => {
    let currentIndex = dataArray.length, temporaryValue, randomIndex;

    // While there are elements remaining to be shuffled...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = dataArray[currentIndex];
        dataArray[currentIndex] = dataArray[randomIndex];
        dataArray[randomIndex] = temporaryValue;
    }

    return dataArray;
};
