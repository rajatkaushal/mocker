module.exports = {
    getConfigMatchers: (configData) => {
        return configData
            ? Object.keys(configData).reduce((acc, cur) => {
                acc[cur] = [configData[cur].toString()];
                return acc;
            }, {})
            : {};
    },

    getMimeType: (type) => {
        return ['json', 'xml'].includes(type) ? `application/${type}` : 'text/plain';
    }
};
