const MockHelper = require('./mock-helper');

module.exports = (expectationConfigs) => {
    const reqConfigs = expectationConfigs.requestConfigs;
    const httpRequest = {
        "path": expectationConfigs.path,
        "method": expectationConfigs.method
    };

    if (reqConfigs && reqConfigs.queryParams && Object.keys(reqConfigs.queryParams).length > 0) {
        httpRequest.queryStringParameters = MockHelper.getConfigMatchers(reqConfigs.queryParams);
    }

    if (reqConfigs && reqConfigs.headers && Object.keys(reqConfigs.headers).length > 0) {
        httpRequest.headers = MockHelper.getConfigMatchers(reqConfigs.headers);
    }

    if (reqConfigs && reqConfigs.cookies && Object.keys(reqConfigs.cookies).length > 0) {
        httpRequest.cookies = reqConfigs.cookies;
    }

    if (reqConfigs && reqConfigs.body) {
        switch (reqConfigs.type) {
            case 'json': {
                httpRequest.body = {
                    type: 'JSON',
                    json: reqConfigs.body
                };
                if (reqConfigs.matchType) {
                    httpRequest.body.matchType = reqConfigs.matchType;
                }
                break;
            }
            case 'xml': {
                httpRequest.body = {
                    type: 'XML',
                    xml: reqConfigs.body
                };
                break;
            }
            default:
                httpRequest.body = reqConfigs.body;
                break;
        }
    }

    return httpRequest;
};
