const onError = {
    log: ({error, errLabel}) => {
        console.error(`${errLabel ? errLabel : 'Following error occurred'} ${error ? '::' : ''} ${error}`);
    },

    throwDataNotFoundError: () => {
        throw {
            status: 404,
            code: 'NOT_FOUND',
            message: 'Data not available'
        };
    },

    throwDBError: (error) => {
        throw {
            status: error.status || 500,
            code: error.code || 'INTERNAL_SERVER_ERROR',
            message: error.message || 'Something went wrong in the server'
        };
    },

    throwDBUpdateError: (error) => {
        if (error.code === 11000)
            throw {
                status: 400,
                code: 'BAD_REQUEST',
                message: 'Model with same name already exists',
                payload: error
            };
        else
            throw {
                status: 400,
                code: 'BAD_REQUEST',
                message: error.message,
                payload: error
            };
    }
};

module.exports = onError;
