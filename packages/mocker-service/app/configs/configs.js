const dotenv = require("dotenv"); // to register environment variables defined in .env file
dotenv.config(); // Registers environment variables defined in .env file

module.exports = {
    environment: process.env.NODE_ENV,
    serverPort: process.env.PORT,
    dbURL: process.env.DB_URL,
    mockServerHost: process.env.MOCK_SERVER_HOST,
    mockServerPort: process.env.MOCK_SERVER_PORT
};
