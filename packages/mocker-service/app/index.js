// ===========================
// import all dependencies ===
// ===========================
const express = require("express"); // most important module. Used to create express app
const mongoose = require("mongoose"); // ORM for mongodb
const compression = require("compression");
const morgan = require('morgan'); // for logging every request on console
const path = require('path'); // for resolving path related queries
const rfs = require("rotating-file-stream"); // for creating new rotational logs file based on file size or time

// import application configs
const configs = require("./configs/configs"); // for accessing global application props defined in .env file

// import error logger
const ErrorUtils = require("./utils/on-error"); // for logging errors

// import mock server files
const startMockServer = require("./services/start-mock-server"); // for starting mock server
const stopMockServer = require("./services/stop-mock-server"); // for stopping mock server

// import all route files
const expectationsAPIs = require('./routes/expectations');
const mockServerAPIs = require('./routes/mocks');


// ===========================
// basic configurations ======
// ===========================
const app = express(); // Creates an Express application
const logStream = rfs.createStream(path.join(__dirname, '..', 'logs', 'access.log'), {
    size: "5M", // rotate every 5 MegaBytes written
    interval: "1d", // rotate daily
    compress: "gzip" // compress rotated files
}); // create a rotational write stream for logger file


// ===========================
// register all middleware ===
// ===========================
app.use(compression()); // use compression to compress every response object (improves performance)
app.use(morgan('combined', { stream: logStream })); // setup morgan logger to log requests
app.use(express.json()); // Parse json content from request body


// ===========================
// base api endpoints ========
// ===========================
app.use('/expectations', expectationsAPIs);
app.use('/mocks', mockServerAPIs);


// ===========================
// error handlers ============
// ===========================

// catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = {
        status: 404,
        code: 'NOT_FOUND',
        message: 'Resource Not Found'
    };
    res.status(err.status).json(err);
    next(err);
});


// ===========================
// db configurations =========
// ===========================

// Using native (ES6) promise instead of deprecated mongoose Promise
mongoose.Promise = global.Promise;

// Connect to db
mongoose.connect(configs.dbURL, {
    poolSize: 10,
    autoIndex: true,
    keepAlive: true,
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true
});

// Connected handler
mongoose.connection.on('connected', () => {
    console.log("Connected to DB");
});

// Error handler
mongoose.connection.on('error', (err) => {
    console.log('Following error occurred while dealing with database :: ', err);
});

// Reconnect when disconnected
mongoose.connection.on('disconnected', () => {
    console.log('Mongoose disconnected. Retrying Connection.....');
    mongoose.createConnection(configs.dbURL, {useNewUrlParser: true});
});

// Reconnect when closed
mongoose.connection.on('close', () => {
    console.log('Mongoose Connection Closed. Retrying Connection.....');
    mongoose.createConnection(configs.dbURL, {useNewUrlParser: true});
});

// Reconnect when error
mongoose.connection.on('error', (error) => {
    console.log('Mongoose Connection Error :: ', error);
    console.log('Retrying Connection.....');
    mongoose.createConnection(configs.dbURL, {useNewUrlParser: true});
});

// Close the Mongoose connection after responding to the request
app.use((req, res, next) => {
    // action after response
    const afterResponse = () => {
        console.log('Closing db connection');

        // any other clean ups
        mongoose.connection.close(function() {
            console.log('Mongoose connection disconnected');
        });

        // hooks to execute after response
        res.on('finish', afterResponse);
        res.on('close', afterResponse);

        next();
    }
});


// ===========================
// Server configurations =====
// ===========================

// Set port to register the app
const port = configs.serverPort || 9999;

// Register the app on above port
const server = app.listen(port);

// Log for successful app registration on specified port
server.on('listening', async () => {
    await startMockServer({port: configs.mockServerPort});
    console.log('Server started on port: ' + port);
    console.log('Mock Server started on port: ' + configs.mockServerPort);
});

// Log for error while registering the app on specified port
server.on('error', (err) => {
    if (err.syscall !== 'listen')
        ErrorUtils.log({error: err, errLabel: 'Following error occurred while starting the app server'});

    // handle specific listen errors with friendly messages
    switch (err.code) {
        case 'EACCES':
            console.error(`Port ${port} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(`Port ${port} is already in use`);
            process.exit(1);
            break;
        default:
            ErrorUtils.log({error: err, errLabel: 'Unknown error occurred while starting the app server'});
    }
});

// cleanup when app is closing
process.on('exit', async () => {
    await cleanupOnServerIssues();
});

// cleanup on ctrl+c event
process.on('SIGINT', async () => {
    await cleanupOnServerIssues();
    process.exit();
});

// cleanup on explicit service kill by "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', async () => {
    await cleanupOnServerIssues();
    process.exit();
});
process.on('SIGUSR2', async () => {
    await cleanupOnServerIssues();
    process.exit();
});

// cleanup when app closed due to uncaught exceptions
process.on('uncaughtException', async () => {
    await cleanupOnServerIssues();
    process.exit();
});

const cleanupOnServerIssues = async () => {
    await mongoose.connection.close();
    await stopMockServer({port: configs.mockServerPort});

    console.log('Mongoose disconnected & stopped mock server since server terminated');
    process.exit(0);
};

// ===========================
// Export app ================
// ===========================
module.exports = app;
