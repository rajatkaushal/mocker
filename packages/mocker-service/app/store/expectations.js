const ExpectationsModel = require('../models/expectations');
const ErrorUtils = require('../utils/on-error');

// partial response fields
const responseFields = ['_id', 'name', 'path', 'method', 'description'];


/**
 * saves an expectation in db
 */
const createExpectation = async (req) => {
    try {
        return await ExpectationsModel.create(req.body);
    } catch (error) {
        ErrorUtils.log({error: error, errLabel: 'Following error occurred while saving the expectation'});
        ErrorUtils.throwDBUpdateError(error);
    }
};


/**
 * gives paginated response of all expectations
 * Query params: page, limit, select, sort, name
 */
const getExpectations = async (req) => {
    const page = req.query.page ? +req.query.page : 0;
    const limit = req.query.limit ? +req.query.limit : 10;

    const options = {
        skip: page * limit,
        limit: limit,
        sort: req.query.sort
    };
    const searchCriteria = req.query.name ? { name: new RegExp(req.query.name, 'ig') } : {};
    const selectFields = req.query.select ? req.query.select.replace(/,/g, ' ') : responseFields.join(' ');

    try {
        const savedExpectations = await ExpectationsModel.find(searchCriteria, selectFields, options);

        if (savedExpectations === null)
            ErrorUtils.throwDataNotFoundError();

        return savedExpectations;
    } catch (error) {
        ErrorUtils.log({error: error, errLabel: 'Following error occurred while retrieving the expectations'});
        ErrorUtils.throwDBError(error);
    }
};


/**
 * Returns an expectation by id
 */
const getExpectationById = async (id) => {
    try {
        const savedExpectation = await ExpectationsModel.findById(id)
            .populate('requestConfigs')
            .populate('responseConfigs');

        if (savedExpectation === null)
            ErrorUtils.throwDataNotFoundError();

        return savedExpectation;
    } catch (error) {
        ErrorUtils.log({error: error, errLabel: 'Following error occurred while retrieving the expectation with ID :: ' + id});
        ErrorUtils.throwDBError(error);
    }
};


/**
 * updates expectation with matching id
 */
const updateExpectation = async (req) => {
    const id = req.path.id || req.params.id;
    delete req.body.id;
    delete req.body._id;

    try {
        return await ExpectationsModel.update(id, req.body);
    } catch (error) {
        ErrorUtils.log({error: error, errLabel: 'Following error occurred while updating the expectation with ID :: ' + id});
        ErrorUtils.throwDBUpdateError(error);
    }
};


/**
 * deletes expectation with matching id
 */
const deleteExpectationById = async (id) => {
    try {
        return await ExpectationsModel.delete(id);
    } catch (error) {
        ErrorUtils.log({error: error, errLabel: 'Following error occurred while deleting the expectation with ID :: ' + id});
        ErrorUtils.throwDBError(error);
    }
};


module.exports = {createExpectation, getExpectations, getExpectationById, updateExpectationById: updateExpectation, deleteExpectationById};
